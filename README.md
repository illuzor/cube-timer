# Cube Timer
[![pipeline status](https://gitlab.com/illuzor/cube-timer/badges/master/pipeline.svg)](https://gitlab.com/illuzor/cube-timer/commits/master) [![ktlint](https://img.shields.io/badge/code%20style-%E2%9D%A4-FF4081.svg)](https://ktlint.github.io/)

### What is it?
This is a [Speedcube](https://en.wikipedia.org/wiki/Speedcubing) Timer application for Android.
This is my pet project and it is in lazy development in free time. And also my playground for new technologies.

### Notable analogues

### Download
* [APK](https://app.illuzor.com/cubetimer/)

### Features to implement
* [ ] Timer
* [ ] Records store and display (latest, best, worst, avg. last 5, avg. last 10)
* [ ] Result sharing
* [ ] Progress graphs
* [ ] Different puzzles support and user defined puzzles
* [ ] Scrumbling patterns for popular puzzles
* [ ] Dark theme

### Modules
* **:app** - main application module
* **:storage** - data storage, database

### Gradle Plugins
* [Jetpack Navigation Safeargs Kotlin](https://developer.android.com/jetpack/androidx/releases/navigation#safe_args)
* [Ktlint Gradle](https://github.com/JLLeitschuh/ktlint-gradle/)
* [detekt](https://github.com/detekt/detekt)

### Libs
* [JUnit 4](https://github.com/junit-team/junit4)
* [Androidx Test Core](https://mvnrepository.com/artifact/androidx.test/core)
* [Robolectric](https://github.com/robolectric/robolectric)
* [MockK](https://github.com/mockk/mockk)
* [Kotlin](https://github.com/JetBrains/kotlin)
* [Kotlinx Coroutines](https://github.com/Kotlin/kotlinx.coroutines)
* [Androidx Core](https://developer.android.com/jetpack/androidx/releases/core)
* [Androidx Appcompat](https://developer.android.com/jetpack/androidx/releases/appcompat)
* [Androidx Recyclerview](https://developer.android.com/jetpack/androidx/releases/recyclerview)
* [Androidx Navigation](https://developer.android.com/jetpack/androidx/releases/navigation)
* [Androidx Room](https://developer.android.com/jetpack/androidx/releases/room)
* [Koin](https://github.com/InsertKoinIO/koin)
* [LeakCanary](https://square.github.io/leakcanary/getting_started/)

### Contributing
Read the [document](https://gitlab.com/illuzor/cube-timer/blob/master/CONTRIBUTING.md)