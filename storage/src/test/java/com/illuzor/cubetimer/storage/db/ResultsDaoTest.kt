package com.illuzor.cubetimer.storage.db

import android.os.Build
import androidx.room.Room
import com.illuzor.cubetimer.storage.context
import com.illuzor.cubetimer.storage.model.ResultEntity
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P])
class ResultsDaoTest {

    private companion object {
        const val CUBE2 = "2x2x2 cube"
        const val CUBE3 = "3x3x3 cube"
        const val CUBE4 = "4x4x4 cube"
    }

    private lateinit var dao: ResultsDao
    private lateinit var db: TimerDatabase

    @Before
    fun initDb() {
        db = Room.inMemoryDatabaseBuilder(context, TimerDatabase::class.java).build()
        dao = db.getResultsDao()
    }

    @After
    fun closeDb() = db.close()

    @Test
    fun insert_and_get_all() = runBlocking {
        val results: List<ResultEntity> = (0 until 10)
            .map { makeResult(it * 100L) }
            .sortedByDescending { it.date }

        results.forEach { dao.insert(it) }

        assertEquals(results, dao.getAllResults(CUBE3).first())
    }

    @Test
    fun insert_and_get_all_in_different_puzzles() = runBlocking {
        val results: List<ResultEntity> = (0 until 10)
            .map { makeResult(it * 100L) }
            .sortedByDescending { it.date }
        val twoByTwo = ResultEntity(0, CUBE2, 888, 100)
        val sixBySix = ResultEntity(0, CUBE4, 999, 100)

        results.forEach { dao.insert(it) }
        dao.insert(twoByTwo)
        dao.insert(sixBySix)

        assertEquals(results, dao.getAllResults(CUBE3).first())
        assertEquals(listOf(twoByTwo), dao.getAllResults(CUBE2).first())
        assertEquals(listOf(sixBySix), dao.getAllResults(CUBE4).first())
    }

    @Test
    fun get_last_results() = runBlocking {
        val results: List<ResultEntity> = (0 until 20).map { makeResult(it * 100L) }

        results.forEach { dao.insert(it) }

        assertEquals(
            results.sortedByDescending { it.date }.take(3),
            dao.getLastResults(CUBE3, 3)
        )
        assertEquals(
            results.sortedByDescending { it.date }.take(5),
            dao.getLastResults(CUBE3, 5)
        )
        assertEquals(
            results.sortedByDescending { it.date }.take(10),
            dao.getLastResults(CUBE3, 10)
        )
        assertEquals(
            results.sortedByDescending { it.date }.take(15),
            dao.getLastResults(CUBE3, 15)
        )
    }

    @Test
    fun get_average_time() = runBlocking {
        dao.insert(makeResult(0))
        dao.insert(makeResult(100))
        dao.insert(makeResult(200))
        dao.insert(makeResult(300))
        dao.insert(makeResult(400))
        dao.insert(makeResult(500))

        assertEquals(500L, dao.getLastAvgTime(CUBE3, 1))
        assertEquals(400L, dao.getLastAvgTime(CUBE3, 3))
        assertEquals(350L, dao.getLastAvgTime(CUBE3, 4))
        assertEquals(300L, dao.getLastAvgTime(CUBE3, 5))
    }

    @Test
    fun get_best_time() = runBlocking {
        val results: List<ResultEntity> = (0 until 10).map { makeResult(it * 100L + 3) }

        results.forEach { dao.insert(it) }
        dao.insert(makeResult(800, CUBE2))
        dao.insert(makeResult(600, CUBE2))

        assertEquals(3, dao.getBestTime(CUBE3))
        assertEquals(600, dao.getBestTime(CUBE2))
    }

    @Test
    fun get_worst_time() = runBlocking {
        val results: List<ResultEntity> = (0 until 10).map { makeResult(it * 100L) }

        results.forEach { dao.insert(it) }
        dao.insert(makeResult(800, CUBE2))
        dao.insert(makeResult(600, CUBE2))

        assertEquals(900, dao.getWorstTime(CUBE3))
        assertEquals(800, dao.getWorstTime(CUBE2))
    }

    @Test
    fun results_count() = runBlocking {
        assertEquals(0, dao.resultsCount(CUBE3))

        dao.insert(makeResult(800, CUBE3))
        dao.insert(makeResult(900, CUBE3))

        assertEquals(2, dao.resultsCount(CUBE3))

        dao.insert(makeResult(100, CUBE2))

        assertEquals(2, dao.resultsCount(CUBE3))
        assertEquals(1, dao.resultsCount(CUBE2))
        assertEquals(0, dao.resultsCount(CUBE4))
    }

    @Test
    fun deleting_result() = runBlocking {
        val result = makeResult(30_000)

        dao.insert(result)

        assertEquals(1, dao.getLastResults(CUBE3, Int.MAX_VALUE).size)

        val allResults: List<ResultEntity> = dao.getAllResults(CUBE3).first()
        dao.delete(allResults[0])

        assertEquals(0, dao.getAllResults(CUBE3).first().size)
    }

    private suspend fun makeResult(
        time: Long,
        puzzle: String = CUBE3,
        delayTime: Long = 10
    ): ResultEntity {
        delay(delayTime)
        return ResultEntity(0, puzzle, time, System.currentTimeMillis())
    }
}
