package com.illuzor.cubetimer.storage.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "results")
data class ResultEntity(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int,

    @ColumnInfo(name = "puzzle")
    val puzzle: String,

    @ColumnInfo(name = "time")
    val time: Long,

    @ColumnInfo(name = "date")
    val date: Long
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ResultEntity

        if (puzzle != other.puzzle) return false
        if (time != other.time) return false
        if (date != other.date) return false

        return true
    }

    override fun hashCode(): Int {
        var result = puzzle.hashCode()
        result = 31 * result + time.hashCode()
        result = 31 * result + date.hashCode()
        return result
    }
}
