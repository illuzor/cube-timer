package com.illuzor.cubetimer.storage.repositories

import com.illuzor.cubetimer.storage.db.ResultsDao
import com.illuzor.cubetimer.storage.model.ResultEntity
import kotlinx.coroutines.flow.Flow

internal class ResultsRepositoryImpl(private val dao: ResultsDao) : ResultsRepository {
    override fun getResults(puzzle: String): Flow<List<ResultEntity>> = dao.getAllResults(puzzle)
}
