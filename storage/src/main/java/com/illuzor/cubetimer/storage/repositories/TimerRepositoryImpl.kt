package com.illuzor.cubetimer.storage.repositories

import com.illuzor.cubetimer.storage.db.ResultsDao
import com.illuzor.cubetimer.storage.model.ResultEntity

internal class TimerRepositoryImpl(private val dao: ResultsDao) : TimerRepository {

    override suspend fun insert(result: ResultEntity) = dao.insert(result)

    override suspend fun getLastAvgTime(puzzle: String, amount: Int) =
        dao.getLastAvgTime(puzzle, amount)

    override suspend fun getLatestTime(puzzle: String) = dao.getLastAvgTime(puzzle, 1)
    override suspend fun getBestTime(puzzle: String) = dao.getBestTime(puzzle)
    override suspend fun getWorstTime(puzzle: String) = dao.getWorstTime(puzzle)
    override suspend fun resultsCount(puzzle: String) = dao.resultsCount(puzzle)
}
