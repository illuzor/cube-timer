package com.illuzor.cubetimer

import android.os.Build
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config

@RunWith(RobolectricTestRunner::class)
@Config(sdk = [Build.VERSION_CODES.P])
class AppTest {

    private companion object {
        const val PACKAGE_NAME = "com.illuzor.cubetimer"
    }

    @Test
    fun useAppContext() {
        assertEquals(PACKAGE_NAME, context.packageName)
    }
}
