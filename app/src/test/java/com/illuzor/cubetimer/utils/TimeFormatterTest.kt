package com.illuzor.cubetimer.utils

import org.junit.Assert.assertEquals
import org.junit.Test

class TimeFormatterTest {

    private val timeFormatter = TimeFormatter()

    @Test(expected = IllegalArgumentException::class)
    fun negative() {
        timeFormatter.format(-222)
    }

    @Test
    fun conversions() {
        assertEquals("00:12.22", timeFormatter.format(12_225))
        assertEquals("00:49.33", timeFormatter.format(49_330))
        assertEquals("00:59.99", timeFormatter.format(59_999))

        assertEquals("13:03.25", timeFormatter.format(60_000L * 13 + 3250))
        assertEquals("54:17.66", timeFormatter.format(60_000L * 54 + 17_666))
        assertEquals("59:59.99", timeFormatter.format(60_000L * 59 + 59_999))

        assertEquals(
            "3:22:44.00",
            timeFormatter.format(3_600_000L * 3 + 60_000 * 22 + 44_000)
        )
        assertEquals(
            "99:59:32.10",
            timeFormatter.format(3_600_000L * 99 + 60_000 * 59 + 32_100)
        )
        assertEquals(
            "99:59:59.99",
            timeFormatter.format(3_600_000L * 99 + 60_000 * 59 + 59_999)
        )
    }

    @Test
    fun zero() {
        assertEquals("00:00.00", timeFormatter.format(0))
    }

    @Test
    fun `milliseconds edge cases`() {
        assertEquals("00:00.00", timeFormatter.format(6))
        assertEquals("00:00.06", timeFormatter.format(60))
        assertEquals("00:00.60", timeFormatter.format(600))
        assertEquals("00:00.99", timeFormatter.format(999))
    }

    @Test
    fun `seconds edge cases`() {
        assertEquals("00:01.00", timeFormatter.format(1000))
        assertEquals("00:59.00", timeFormatter.format(59_000))
    }

    @Test
    fun `minutes edge cases`() {
        assertEquals("01:00.00", timeFormatter.format(60_000))
        assertEquals("59:00.00", timeFormatter.format(60_000L * 59))
    }

    @Test
    fun `hours edge cases`() {
        assertEquals("1:00:00.00", timeFormatter.format(3_600_000))
        assertEquals("999:00:00.00", timeFormatter.format(3_596_400_000))
    }
}
