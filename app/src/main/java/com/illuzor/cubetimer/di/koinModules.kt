package com.illuzor.cubetimer.di

import androidx.navigation.NavController
import com.illuzor.cubetimer.screens.RootRouter
import com.illuzor.cubetimer.screens.results.ResultsRouter
import com.illuzor.cubetimer.screens.results.ResultsViewModel
import com.illuzor.cubetimer.screens.timer.TimerRouter
import com.illuzor.cubetimer.screens.timer.TimerViewModel
import com.illuzor.cubetimer.storage.di.storageModule
import com.illuzor.cubetimer.utils.InstanceHolder
import com.illuzor.cubetimer.utils.TimeFormatter
import com.illuzor.cubetimer.utils.Timer
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

private val singleModule = module {
    single { InstanceHolder<NavController>() }
    single { RootRouter(get()) }
    single { TimeFormatter() }
}

private val timerModule = module {
    factory { Timer() }
    factory { TimerRouter(get()) }
    viewModel { TimerViewModel(get(), get(), get(), get()) }
}

private val resultsModule = module {
    factory { ResultsRouter(get()) }
    viewModel { ResultsViewModel(get(), get()) }
}

val koinModules = listOf(storageModule, singleModule, timerModule, resultsModule)
