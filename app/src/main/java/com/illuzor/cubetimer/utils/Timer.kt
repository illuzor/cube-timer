package com.illuzor.cubetimer.utils

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.Default
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlin.time.ExperimentalTime
import kotlin.time.TimeSource

class Timer(private val tickDelay: Long = 10) {

    var isActive: Boolean = false
        private set

    var milliseconds: Long = 0
        private set

    private lateinit var timerJob: Job
    private lateinit var onTickListener: (Long) -> Unit

    @Throws(IllegalStateException::class)
    fun start() {
        check(::onTickListener.isInitialized) {
            "Add tick listener before start by the onTick() method."
        }

        check(!isActive) {
            "Timer is already started"
        }

        startTimer()
        isActive = true
    }

    @OptIn(ExperimentalTime::class)
    private fun startTimer() {
        milliseconds = 0
        val startTime = TimeSource.Monotonic.markNow()

        timerJob = CoroutineScope(Default).launch {
            while (true) {
                delay(tickDelay)
                milliseconds = startTime.elapsedNow().inWholeMilliseconds
                onTickListener(milliseconds)
            }
        }
    }

    @Throws(IllegalStateException::class)
    fun stop() {
        check(isActive) {
            "Timer is not started. Start the timer first."
        }
        timerJob.cancel()
        isActive = false
    }

    fun onTick(listener: (Long) -> Unit) {
        onTickListener = listener
    }
}
