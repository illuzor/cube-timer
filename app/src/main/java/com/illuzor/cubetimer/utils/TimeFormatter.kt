package com.illuzor.cubetimer.utils

class TimeFormatter {

    private companion object {
        const val MS_IN_SECOND = 1000
        const val MS_IN_MINUTE = MS_IN_SECOND * 60
        const val MS_IN_HOUR = MS_IN_MINUTE * 60
    }

    @Throws(IllegalArgumentException::class)
    fun format(milliseconds: Long): String {
        require(milliseconds >= 0) {
            "Time can`t be negative"
        }

        val ms = milliseconds % MS_IN_SECOND
        val sec = milliseconds % MS_IN_MINUTE / MS_IN_SECOND
        val min = milliseconds % MS_IN_HOUR / MS_IN_MINUTE
        val hours = milliseconds / MS_IN_HOUR

        val result = String.format("%02d:%02d.%2.2s", min, sec, String.format("%03d", ms))

        return if (hours > 0) {
            "$hours:$result"
        } else {
            result
        }
    }
}
