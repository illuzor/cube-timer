package com.illuzor.cubetimer.utils

class InstanceHolder<T> {

    val hasValue: Boolean
        get() = instance != null

    val value: T
        get() = instance!!

    private var instance: T? = null

    fun setValue(instance: T) {
        this.instance = instance
    }

    fun cleanup() {
        instance = null
    }
}
