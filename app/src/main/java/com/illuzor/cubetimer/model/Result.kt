package com.illuzor.cubetimer.model

data class Result(val id: Int, val puzzle: String, val time: String, val date: String)
