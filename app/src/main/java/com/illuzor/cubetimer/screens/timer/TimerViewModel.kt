package com.illuzor.cubetimer.screens.timer

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.illuzor.cubetimer.model.RecentResults
import com.illuzor.cubetimer.storage.model.ResultEntity
import com.illuzor.cubetimer.storage.repositories.TimerRepository
import com.illuzor.cubetimer.utils.TimeFormatter
import com.illuzor.cubetimer.utils.Timer
import kotlinx.coroutines.async
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import java.lang.System.currentTimeMillis

class TimerViewModel(
    private val timer: Timer,
    private val formatter: TimeFormatter,
    private val repository: TimerRepository,
    private val router: TimerRouter,
) : ViewModel() {

    private companion object {
        const val FIVE_RESULTS = 5
        const val TEN_RESULTS = 10
    }

    private val currentPuzzle = "3x3x3 cube"

    private val innerRecentResults = MutableStateFlow(RecentResults.empty())
    val recentResultsFlow: StateFlow<RecentResults> by ::innerRecentResults

    private val innerTimerFlow = MutableStateFlow("no result")
    val timerFlow: StateFlow<String> by ::innerTimerFlow

    init {
        updateLatestResults()

        timer.onTick {
            innerTimerFlow.value = format(it)
        }
    }

    fun toggleTimer() {
        if (timer.isActive) {
            timer.stop()
            viewModelScope.launch {
                repository.insert(
                    ResultEntity(0, currentPuzzle, timer.milliseconds, currentTimeMillis())
                )
            }
            updateLatestResults()
        } else {
            timer.start()
        }
    }

    fun showResults() = router.toResults()

    override fun onCleared() {
        if (timer.isActive) {
            timer.stop()
        }
    }

    private fun updateLatestResults() {
        viewModelScope.launch {
            if (repository.resultsCount(currentPuzzle) == 0) {
                return@launch
            }

            val latestResult = async { repository.getLatestTime(currentPuzzle) }
            val bestResult = async { repository.getBestTime(currentPuzzle) }
            val worstResult = async { repository.getWorstTime(currentPuzzle) }
            val avgLast5Result = async { repository.getLastAvgTime(currentPuzzle, FIVE_RESULTS) }
            val avgLast10Result = async { repository.getLastAvgTime(currentPuzzle, TEN_RESULTS) }

            val results = RecentResults(
                latest = format(latestResult.await()),
                best = format(bestResult.await()),
                worst = format(worstResult.await()),
                avgLast5 = format(avgLast5Result.await()),
                avgLast10 = format(avgLast10Result.await())
            )
            innerRecentResults.value = results
        }
    }

    @Suppress("NOTHING_TO_INLINE")
    private inline fun format(milliseconds: Long): String = formatter.format(milliseconds)
}
