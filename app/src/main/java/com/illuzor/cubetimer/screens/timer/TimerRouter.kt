package com.illuzor.cubetimer.screens.timer

import com.illuzor.cubetimer.screens.RootRouter

class TimerRouter(private val rootRouter: RootRouter) {

    fun toResults() = rootRouter.navigate(TimerFragmentDirections.toResultsFragment())
}
