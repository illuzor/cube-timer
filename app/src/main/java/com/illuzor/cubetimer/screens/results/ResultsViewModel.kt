package com.illuzor.cubetimer.screens.results

import androidx.lifecycle.ViewModel
import com.illuzor.cubetimer.model.Result
import com.illuzor.cubetimer.storage.model.ResultEntity
import com.illuzor.cubetimer.storage.repositories.ResultsRepository
import com.illuzor.cubetimer.utils.TimeFormatter
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class ResultsViewModel(
    private val formatter: TimeFormatter,
    private val repository: ResultsRepository,
) : ViewModel() {

    fun getResults(puzzle: String): Flow<List<Result>> =
        repository.getResults(puzzle)
            .map { list ->
                list.map { it.toResult() }
            }

    private fun ResultEntity.toResult(): Result {
        val date = SimpleDateFormat("dd.MM.yyyy", Locale.getDefault()).format(Date(this.date))
        return Result(this.id, this.puzzle, formatter.format(this.time), date)
    }
}
