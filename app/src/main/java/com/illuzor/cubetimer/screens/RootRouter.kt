package com.illuzor.cubetimer.screens

import androidx.navigation.NavController
import androidx.navigation.NavDirections
import com.illuzor.cubetimer.utils.InstanceHolder

class RootRouter(private val navControllerHolder: InstanceHolder<NavController>) {

    private val navController: NavController
        get() = navControllerHolder.value

    fun navigate(direction: NavDirections) = navController.navigate(direction)

    fun goBack() = navController.popBackStack()
}
