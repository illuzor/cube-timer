package com.illuzor.cubetimer.screens.results

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import org.koin.androidx.viewmodel.ext.android.viewModel

@Suppress("MagicNumber")
class ResultFragment : Fragment() {

    private val viewModel: ResultsViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = ComposeView(requireContext()).apply {
        setContent {
            MaterialTheme {
                ResultsScreen()
            }
        }
    }

    @Composable
    fun ResultsScreen() {
        val resultsState =
            viewModel.getResults("3x3x3 cube").collectAsState(initial = emptyList())

        LazyColumn {
            items(resultsState.value) { result ->
                Result(result.puzzle, result.time, result.date)
            }
        }
    }

    @Composable
    fun Result(
        puzzle: String,
        time: String,
        date: String,
    ) {
        Row {
            Text(text = puzzle)
            Text(text = time, modifier = Modifier.padding(start = 8.dp))
            Text(text = date, modifier = Modifier.padding(start = 8.dp))
        }
    }
}
