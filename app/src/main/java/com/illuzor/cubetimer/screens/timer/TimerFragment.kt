package com.illuzor.cubetimer.screens.timer

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Button
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.State
import androidx.compose.runtime.collectAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.ComposeView
import androidx.compose.ui.unit.dp
import androidx.fragment.app.Fragment
import com.illuzor.cubetimer.model.RecentResults
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.koin.androidx.viewmodel.ext.android.viewModel

@ExperimentalCoroutinesApi
@Suppress("MagicNumber")
class TimerFragment : Fragment() {

    private val viewModel: TimerViewModel by viewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = ComposeView(requireContext()).apply {
        setContent {
            MaterialTheme {
                TimerScreen()
            }
        }
    }

    @Composable
    fun TimerScreen() {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            val time = viewModel.timerFlow.collectAsState()

            Text(text = time.value, modifier = Modifier.padding(16.dp))
            Button(onClick = viewModel::toggleTimer) {
                Text(text = "Timer Button")
            }
            val results: State<RecentResults> = viewModel.recentResultsFlow.collectAsState()
            RecentResults(results.value)

            Button(onClick = viewModel::showResults, modifier = Modifier.padding(top = 16.dp)) {
                Text(text = "Results")
            }
        }
    }

    @Composable
    fun RecentResults(results: RecentResults) {
        Column(horizontalAlignment = Alignment.CenterHorizontally) {
            Result(name = "Latest", value = results.latest)
            Result(name = "Best", value = results.best, color = Color.Green)
            Result(name = "Worst", value = results.worst, color = Color.Red)
            Result(name = "Average of last 5", value = results.avgLast5)
            Result(name = "Average of last 10", value = results.avgLast10)
        }
    }

    @Composable
    fun Result(name: String, value: String, color: Color = Color.Black) {
        val padding = Modifier.padding(vertical = 8.dp, horizontal = 4.dp)
        Row {
            Text(text = "$name:", modifier = padding, color = color)
            Text(text = value, modifier = padding)
        }
    }
}
